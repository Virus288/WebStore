.basketMenuOutter {
  @include flexCenter(column);
  .basketSubmit {
    text-align: center;
    margin: 0 auto;
    button {
      @include OhOrangeButton();
      width: 200px;
      &:hover {
        box-shadow: none;
        width: 150px;
        transition: 0.5s all ease-in-out;
      }
    }
  }
}

.basketMenuInner {
  max-width: 1200px;
  margin: 0 auto;
  @include flexCenter(row);
  .productDiv {
    text-align: center;
    min-height: 500px;
    height: auto;
    h4 {
      margin: 0 auto;
      cursor: pointer;
      color: red;
    }
  }
  form {
    flex-direction: column;
    max-width: 500px;
  }
}

.basketComponentOuterDiv {
  margin: 5px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  flex-wrap: wrap;
  @media only screen and (min-width:900px){
    justify-content: center;
    width: 100%;
  }
  @media only screen and (max-width:900px){
    justify-content: center;
    width: 100%;
  }
}

.basketProductsDiv {
  @include flexCenter(row);
  @media only screen and (min-width:900px){
    width: 50%;
  }
  @media only screen and (max-width:900px){
    width: 100%;
  }
}

.basketProductDiv {
  min-width: 300px;
  max-width: 400px;
  min-height: 200px;
  max-height: 400px;
  background: rgba($color: white, $alpha: 0.5);
  margin: 10px;
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: nowrap;
  border-radius: 10px;
  h2 {
    width: 40%;
  }
  h3 {
    font-size: 1.22em;
    margin-bottom: 5px;
  }
  img {
    align-self: center;
    width: 30%;
    border-radius: 10px;
  }
  .productDivName {
    align-self: center;
    margin-top: 10px;
  }
  .productDivPrice {
    width: 90%;
    text-align: right;
    border-bottom: 1px solid grey;
  }
  span {
    align-self: flex-start;
  }
  .productDivAmount, input {
    display: inline-block;
  }
  input {
    margin-left: 10px;
    font-size: 1.3em;
    outline: none;
    background: rgba($color: grey, $alpha: 0.2);
    border: none;
    text-align: left;
    border-bottom: 1px solid grey;
    transition: 0.4s all ease-in-out;
    &:hover{
      background: rgba($color: grey, $alpha: 0.4);
    }
    &:focus {
      color: black;
      background: rgba($color: grey, $alpha: 0.4);
      padding: 5px;
    }
  }
  @media only screen and (min-width:900px){
    width: 80%;
  }
  @media only screen and (max-width:900px){
    width: 100%;
  }
}

.BasketProductButton {
  align-self: center;
  @include OhOrangeButton();
  background: grey;
  color: white;
}

.disabledButton {
  background: darkgrey !important;
  color: black !important;
  cursor: not-allowed !important;
  transition: 0.5s all ease-in-out
}

.userDataForm {
  @include flexCenter(column);
  label {
    font-size: 1.3em;
    letter-spacing: 1px;
  }
  input {
    font-size: 1.3em;
    outline: none;
    background: none;
    border: none;
    text-align: left;
    border-bottom: 1px solid grey;
    transition: 0.4s all ease-in-out;
    &::placeholder {
      text-align: left;
    }
    &:focus {
      color: black;
      box-shadow: 5px 5px 1px $OhOrange;
      background: rgba($color: grey, $alpha: 0.4);
      padding: 5px;
      &::placeholder {
        color: black;
      }
    }
  }
  button {
    @include OhOrangeButton();
  }
  * {
    text-align: center;
    margin-top: 10px;
    margin-bottom: px;
  }
}

.basketSummarySpan {
  h2 {
    display: inline;
  }
  button {
    display: block;
    @include OhOrangeButton();
  }
}

.basketSummaryDiv {
  @media only screen and (min-width:900px){
    width: 40%;
  }
  @media only screen and (max-width:900px){
    width: 100%;
  }
}