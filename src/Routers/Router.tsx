import React from 'react';
import { Route, Switch, useLocation } from 'react-router-dom';
import { Home } from '../Components/Home/Home';
import { AccountManager } from '../Components/Account/NotLoggedUserComponents/AccountManager';
import { Products } from '../Components/Products/Products';
import { BasketComponent } from '../Components/Basket/Views/BasketComponents';
import { FourOhFour } from '../Components/FourOhFour/FourOhFour';

export const Routers: React.FC = () => {
  const location = useLocation();
  return (
    <Switch location={location} key={location.key}>
      <Route exact path="/" render={(): JSX.Element => <Home />} />
      <Route exact path="/konto" render={(): JSX.Element => <AccountManager {...{ component: 'login' }} />} />
      <Route exact path="/logowanie" render={(): JSX.Element => <AccountManager {...{ component: 'login' }} />} />
      <Route exact path="/rejestracja" render={(): JSX.Element => <AccountManager {...{ component: 'register' }} />} />
      <Route
        exact
        path="/posciele"
        render={(): JSX.Element => <Products {...{ categoryType: 'posciele', categoryName: 'Pościele' }} />}
      />
      <Route
        exact
        path="/przescieradla"
        render={(): JSX.Element => <Products {...{ categoryType: 'przescieradla', categoryName: 'Prześcieradła' }} />}
      />
      <Route
        exact
        path="/poduszki"
        render={(): JSX.Element => <Products {...{ categoryType: 'poduszki', categoryName: 'Poduszki' }} />}
      />
      <Route
        exact
        path="/koce"
        render={(): JSX.Element => <Products {...{ categoryType: 'koce', categoryName: 'Koce' }} />}
      />
      <Route
        exact
        path="/koldry"
        render={(): JSX.Element => <Products {...{ categoryType: 'koldry', categoryName: 'Kołdry' }} />}
      />
      <Route
        exact
        path="/reczniki"
        render={(): JSX.Element => <Products {...{ categoryType: 'reczniki', categoryName: 'Ręczniki' }} />}
      />
      <Route exact path="/koszyk" render={(): JSX.Element => <BasketComponent />} />
      {/* 404 */}
      <Route path="*" render={(): JSX.Element => <FourOhFour />} />
    </Switch>
  );
};
