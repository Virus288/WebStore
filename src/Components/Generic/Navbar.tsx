import { Link, useHistory } from 'react-router-dom';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { motion } from 'framer-motion';

// Internal modules
import { BasketDrop } from '../Basket/Views/BasketDrop';

// Animation
import { slideRight } from '../Animation/Variables';

// Redux
import { disableProductView } from '../../Redux/actions';

export const Navbar: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    listenForNav();
    document.querySelectorAll('.navlink').forEach((link) => {
      link.addEventListener('click', () => {
        hideProductView();
      });
    });
  });

  const showBasket = (): void => {
    if (window.innerWidth < 900) {
      history.push({
        pathname: '/koszyk',
      });
    } else {
      document.querySelector('.basketDrop')?.classList.toggle('basketDrop-active');
    }
  };

  const listenForNav = (): void => {
    window.addEventListener('resize', () => {
      if (window.innerWidth > 900) {
        if (document.querySelector('.navbar-active') === null) {
          document.querySelector('.navbar')?.classList.toggle('navbar-active');
          document.querySelector('.navbar')?.classList.toggle('navbar-inactive');
        }
      }
    });
  };

  const toggleNav = (): void => {
    if (window.innerWidth < 900) {
      document.querySelector('.navbar')?.classList.toggle('navbar-active');
      document.querySelector('.navbar')?.classList.toggle('navbar-inactive');

      document.querySelector('.burger')?.classList.toggle('burger-active');
    }
  };

  const hideProductView = (): void => {
    toggleNav();
    dispatch(disableProductView());
  };

  return (
    <>
      <motion.div className="navbar navbar-inactive" variants={slideRight} initial="init" animate="visible">
        <motion.span
          className="logo"
          initial={{
            x: -250,
          }}
          animate={{
            x: 0,
          }}
          transition={{
            delay: 2,
            type: 'spring',
            stiffness: 50,
          }}
        >
          <Link to="/" style={{ textDecoration: 'none' }}>
            <h5>Capri</h5>
          </Link>
        </motion.span>
        <span className="links">
          <Link to="/posciele" style={{ textDecoration: 'none' }}>
            <div className="navlink">
              <h5>Pościele</h5>
            </div>
          </Link>
          <Link to="/przescieradla" style={{ textDecoration: 'none' }}>
            <div className="navlink">
              <h5>Prześcieradła</h5>
            </div>
          </Link>
          <Link to="/poduszki" style={{ textDecoration: 'none' }}>
            <div className="navlink">
              <h5>Poduszki</h5>
            </div>
          </Link>
          <Link to="/koce" style={{ textDecoration: 'none' }}>
            <div className="navlink">
              <h5>Koce</h5>
            </div>
          </Link>
          <Link to="/koldry" style={{ textDecoration: 'none' }}>
            <div className="navlink">
              <h5>Kołdry</h5>
            </div>
          </Link>
          <Link to="/reczniki" style={{ textDecoration: 'none' }}>
            <div className="navlink">
              <h5>Ręczniki</h5>
            </div>
          </Link>
        </span>
        <span className="navAccount" onClick={toggleNav}>
          <i className="icon-basket navIcon" onClick={showBasket}>
            {' '}
          </i>
          <Link to="/konto" style={{ textDecoration: 'none' }}>
            <i className="icon-adult navIcon loginIcon"> </i>
          </Link>
        </span>
      </motion.div>
      <div className="burger" onClick={hideProductView}>
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
      <div className="header">
        <Link to="/" style={{ textDecoration: 'none' }}>
          <h5 className="navLogo">Capri</h5>
        </Link>
      </div>
      <BasketDrop />
    </>
  );
};

// const isAdmin = (role) => {
//     if(role){
//         return (
//             <>
//                 <Link to="/adminpanel" style={{ textDecoration: "none" }}>
//                     <div className="navlink"><h5>Panel admina</h5></div>
//                 </Link>
//             </>
//         )
//     }
// }
