import { LoginCallbackInterface, RegisterData } from '../Types/loginTypes';

export const checkIfLogged = async (): Promise<LoginCallbackInterface> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/logowanie`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
  });
  const postData = await res.json();
  if (res.ok) {
    return postData;
  } else {
    throw res;
  }
};

export const sendLogin = async (email: string, password: string): Promise<LoginCallbackInterface> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/logowanie`, {
    method: 'POST',
    body: JSON.stringify({ email, password }),
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
  });
  const loginData = await res.json();
  if (res.ok) {
    return loginData;
  } else {
    throw res;
  }
};

export const sendRegister = async (data: RegisterData): Promise<{ message: string }> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/rejestracja`, {
    method: 'POST',
    body: JSON.stringify({
      username: data.name,
      email: data.email,
      password: data.password,
      password2: data.password2,
    }),
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
  });
  const registerData = await res.json();
  if (res.ok) {
    return registerData;
  } else {
    throw res;
  }
};

export const sendLogout = async (): Promise<void> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/wyloguj`, {
    method: 'GET',
    credentials: 'include',
  });
  if (!res.ok) {
    throw res;
  }
};
