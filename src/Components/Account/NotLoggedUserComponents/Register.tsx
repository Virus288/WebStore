import React from 'react';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { useHistory } from 'react-router';
import { submitForm } from './Mechanic/registerFunctions';
import { slideBottom } from '../../Animation/Variables';

export const Register: React.FC = () => {
  const history = useHistory();

  return (
    <motion.div className="accountForm" variants={slideBottom} initial="init" animate="visible" exit="exit">
      <form onSubmit={async (e): Promise<void> => submitForm(e, history)}>
        <h2 className="loginHelperHeader">Rejestracja</h2>

        <div className="input">
          <label htmlFor="username">Imię</label>
          <input type="text" name="username" placeholder="Nazwa użytkownika" required />
        </div>
        <div className="name error" />

        <div className="input">
          <label htmlFor="email">Email</label>
          <input type="email" name="email" placeholder="Email" required />
        </div>
        <div className="email error" />

        <div className="input">
          <label htmlFor="password">Hasło</label>
          <input type="password" name="password" placeholder="Hasło" required />
        </div>
        <div className="password error" />

        <div className="input">
          <label htmlFor="password2">Potwórz hasło</label>
          <input type="password" name="password2" placeholder="Powtórz hasło" required />
        </div>
        <div className="password2 error" />

        <button>Zarejestruj</button>
      </form>
      <h1>
        Posiadasz już konto?
        <Link to={'/logowanie'} style={{ display: 'block' }}>
          Zaloguj
        </Link>
      </h1>
    </motion.div>
  );
};
