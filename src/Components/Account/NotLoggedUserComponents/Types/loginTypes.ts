export interface LoginCallbackInterface {
  role: string;
  username: string;
  verified: boolean;
}

export interface LoginData {
  email: string;
  password: string;
}

export interface LoginErrors {
  user: string;
  password: string;
}

export interface LoginSuccess {
  role: string;
  username: string;
  error: string;
  errors: Error;
  verified: boolean;
}

export interface AccountProps {
  component: string;
}

export interface RegisterData {
  name: string;
  email: string;
  password: string;
  password2: string;
}

export interface RegisterErrors {
  username: string;
  email: string;
  password: string;
  password2: string;
}

export interface RegisterSuccess {
  message: string;
  error: string;
  errors: Error;
}
