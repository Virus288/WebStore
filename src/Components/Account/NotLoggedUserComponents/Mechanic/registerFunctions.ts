import { RegisterData, RegisterErrors } from '../Types/loginTypes';
import React from 'react';
import { History } from 'history';
import { sendRegister } from '../Handlers/loginHandlers';

const handleRegister = (data: RegisterData, history: History): void => {
  sendRegister(data)
    .then(() => {
      return history.push({
        pathname: '/logowanie',
        state: { registered: true },
      });
    })
    .catch((err) => console.log(err));
};

export const submitForm = (e: React.FormEvent<EventTarget>, history: History): void => {
  e.preventDefault();
  const target = e.target as HTMLFormElement;
  const name = target.username.value;
  const email = target.email.value;
  const password = target.password.value;
  const password2 = target.password2.value;

  // Reset errors
  const nameError = document.querySelector('.name.error') as HTMLElement;
  const emailError = document.querySelector('.email.error') as HTMLElement;
  const passwordError = document.querySelector('.password.error') as HTMLElement;

  nameError.innerHTML = ' ';
  emailError.innerHTML = ' ';
  passwordError.innerHTML = ' ';

  return handleRegister({ name, email, password, password2 }, history);
};

export const displayErrors = (errors: RegisterErrors[]): void => {
  const userError = document.querySelector('.name.error') as HTMLElement;
  const emailError = document.querySelector('.email.error') as HTMLElement;
  const passwordError = document.querySelector('.password.error') as HTMLElement;
  const password2Error = document.querySelector('.password2.error') as HTMLElement;

  errors.forEach((error) => {
    switch (Object.keys(error)[0]) {
      case 'username':
        if (userError) userError.textContent = error.username;
        break;
      case 'email':
        if (emailError) emailError.textContent = error.email;
        break;
      case 'password':
        if (passwordError) passwordError.textContent = error.password;
        break;
      case 'password2':
        if (password2Error) password2Error.textContent = error.password2;
        break;
      default:
        break;
    }
  });
};
