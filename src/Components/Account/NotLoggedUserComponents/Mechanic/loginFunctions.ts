import { Dispatch } from 'redux';
import { LoginData, LoginErrors } from '../Types/loginTypes';
import { LogInActionInterface } from '../../../../Redux/types/reduxTypes';
import { checkIfLogged, sendLogin } from '../Handlers/loginHandlers';
import { logIn } from '../../../../Redux/actions';
import React from 'react';

export const handleCheckIfLogged = (dispatch: Dispatch<LogInActionInterface>): void => {
  checkIfLogged()
    .then((response) => {
      return dispatch(logIn(response.role, response.verified, response.username));
    })
    .catch((err) => console.log(err));
};

const handleLogIn: (data: LoginData, dispatch: Dispatch<LogInActionInterface>) => void = (
  data,
  dispatch: Dispatch<LogInActionInterface>,
) => {
  sendLogin(data.email, data.password)
    .then((response) => {
      return dispatch(logIn(response.role, response.verified, response.username));
    })
    .catch((err) => console.log(err));
};

export const submitForm = (e: React.FormEvent, dispatch: Dispatch<LogInActionInterface>): void => {
  e.preventDefault();
  const target = e.target as HTMLFormElement;
  const email = target.email.value;
  const password = target.password.value;

  // Reset errors
  const emailError = document.querySelector('.email.error') as HTMLElement;
  const passwordError = document.querySelector('.password.error') as HTMLElement;
  emailError.textContent = '';
  passwordError.textContent = '';

  handleLogIn({ email, password }, dispatch);
};

export const displayLoginErrors = (errors: LoginErrors[]): void => {
  errors.forEach((error) => {
    const emailError = document.querySelector('.email.error') as HTMLElement;
    const passwordError = document.querySelector('.password.error') as HTMLElement;

    switch (Object.keys(error)[0]) {
      case 'user':
        if (emailError) emailError.textContent = error.user;
        break;
      case 'password':
        if (passwordError) passwordError.textContent = error.password;
        break;
      default:
        break;
    }
  });
};
