import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { useDispatch } from 'react-redux';
import { disablePopup, enablePopup } from '../../../Redux/actions';
import { slideBottom } from '../../Animation/Variables';
import { submitForm } from './Mechanic/loginFunctions';

export const Login: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    if (window.history.state?.state?.registered) {
      dispatch(enablePopup('Zarejestrowano poprawnie. Zaloguj się by kontynuować'));
      setTimeout(() => {
        dispatch(disablePopup());
        window.history.state.state = undefined;
      }, 3500);
    }
  });

  return (
    <motion.div className="accountForm" variants={slideBottom} initial="init" animate="visible" exit="exit">
      <form onSubmit={async (e): Promise<void> => submitForm(e, dispatch)}>
        <h2 className="loginHelperHeader">Logowanie</h2>

        <div className="input">
          <label htmlFor="email">Email</label>
          <input type="email" name="email" placeholder="Email" required />
        </div>
        <div className="email error" />

        <div className="input">
          <label htmlFor="email">Hasło</label>
          <input type="password" name="password" placeholder="Hasło" required />
        </div>
        <div className="password error" />

        <button>Zaloguj</button>
      </form>
      <h1>
        Nie posiadasz konta?
        <Link to={'/rejestracja'} style={{ display: 'block' }}>
          Zarejestruj
        </Link>
      </h1>
    </motion.div>
  );
};
