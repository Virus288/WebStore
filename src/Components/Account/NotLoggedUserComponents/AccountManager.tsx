import React, { useEffect, useState } from 'react';
import { rootState } from '../../../index';
import { useSelector } from 'react-redux';
import { AnimatePresence } from 'framer-motion';
import { Login } from './Login';
import { Register } from './Register';
import { AccountView } from '../LoggedUserComponents/AccountView';
import { AccountProps } from './Types/loginTypes';

export const AccountManager: React.FC<AccountProps> = (data) => {
  const isLogged = useSelector((state: rootState) => state.isLogged.role);
  const [currentComponent, setCurrentComponent] = useState('login');

  useEffect(() => {
    setCurrentComponent(data.component);
  }, []);

  const switchCurrentComponent = (): JSX.Element => {
    if (currentComponent === 'login') {
      return <Login />;
    } else if (currentComponent === 'register') {
      return <Register />;
    } else {
      return <Login />;
    }
  };

  switch (isLogged) {
    case 'notLogged':
      return (
        <div className="loginOuterContainer">
          <div className="loginMainHeader">
            <h1>Zaloguj się by w pełni korzystać z naszego sklepu</h1>
          </div>
          <AnimatePresence exitBeforeEnter>{switchCurrentComponent()}</AnimatePresence>
        </div>
      );
    case 'user':
      return <AccountView />;
    case 'admin':
      return <AccountView />;
    default:
      return (
        <div className="loginOuterContainer">
          <div className="loginMainHeader">
            <h1>Zaloguj się by w pełni korzystać z naszego sklepu</h1>
          </div>
          <AnimatePresence exitBeforeEnter>{switchCurrentComponent()}</AnimatePresence>
        </div>
      );
  }
};
