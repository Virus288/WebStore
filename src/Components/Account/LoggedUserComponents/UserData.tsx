import React from 'react';
import { UserAccountData } from './Types/loginTypes';

export const UserData: React.FC<UserAccountData> = (userData) => {
  return (
    <div>
      Twoje dane: email: {userData.email}
      Miejscowość: {userData.city}
      Ulica: {userData.street}
      Numer budynku: {userData.buildingNumber}
      Numer mieszkania: {userData.apartmentNumber}
      Kod pocztowy: {userData.postCode}
    </div>
  );
};
