import React from 'react';

export const AccountWelcomeScreen: React.FC<{ name: string }> = (data) => {
  return <div>Witaj {data.name}</div>;
};
