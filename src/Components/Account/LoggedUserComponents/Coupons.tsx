import { AnimatePresence } from 'framer-motion';
import React from 'react';

export const Coupons: React.FC = () => {
  return (
    <AnimatePresence exitBeforeEnter>
      <div className="centeredText">
        <h2>Aktualnie nie posiadasz żadnych kuponów rabatowych do użycia</h2>
      </div>
    </AnimatePresence>
  );
};
