import { AnimatePresence } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { rootState } from '../../../index';
import { UserData } from './UserData';
import { BoughtProducts } from './BoughtProducts';
import { sendLogout } from '../NotLoggedUserComponents/Handlers/loginHandlers';
import { addUserData, logOut } from '../../../Redux/actions';
import { AccountWelcomeScreen } from './AccountWelcomeScreen';
import { Coupons } from './Coupons';
import { getUserData } from './Handlers/loginHandlers';
import { UserAccountData } from './Types/loginTypes';

export const AccountView: React.FC = () => {
  const dispatch = useDispatch();
  const localUserData = useSelector((state: rootState) => state.userData);

  const username = useSelector((state: rootState) => state.isLogged.username);
  const [currentScreen, setCurrentScreen] = useState<null | string>(null);
  const [userData, setUserData] = useState<UserAccountData>(localUserData);

  useEffect(() => {
    localUserData.loaded
      ? setUserData(localUserData)
      : getUserData().then((data) => {
          setUserData(data);
          dispatch(addUserData(data));
          console.log(data);
        });
  });

  const renderComponents = (): JSX.Element => {
    switch (currentScreen) {
      case null:
        return <AccountWelcomeScreen name={username} />;
      case 'userData':
        return <UserData {...userData} />;
      case 'boughtItems':
        return <BoughtProducts />;
      case 'coupons':
        return <Coupons />;
      default:
        return <AccountWelcomeScreen name={username} />;
    }
  };

  return (
    <div className="loginOuterContainer">
      <div className="loggedLeftPage">
        <h2>{username}</h2>
        <button className="loginPanelButton" onClick={(): void => setCurrentScreen('userData')}>
          Moje dane
        </button>
        <button className="loginPanelButton" onClick={(): void => setCurrentScreen('boughtItems')}>
          Historia zakupów
        </button>
        <button className="loginPanelButton" onClick={(): void => setCurrentScreen('coupons')}>
          Kupony rabatowe
        </button>
        <button
          className="loginPanelButton"
          onClick={(): void => {
            sendLogout().then(() => dispatch(logOut()));
          }}
        >
          Wyloguj
        </button>
      </div>
      <AnimatePresence exitBeforeEnter>
        <div className="LoggedRightPage">{renderComponents()}</div>
      </AnimatePresence>
    </div>
  );
};
