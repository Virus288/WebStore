import { UserAccountData } from '../Types/loginTypes';

export const getUserData = async (): Promise<UserAccountData> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/konto`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
  });
  const postData = await res.json();
  if (res.ok) {
    return postData;
  } else {
    throw res;
  }
};
