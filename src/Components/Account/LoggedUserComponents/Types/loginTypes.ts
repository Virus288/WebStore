export interface UserDataInterface {
  email: string;
}

export interface UserAccountData {
  email: string;
  city: string;
  street: string;
  buildingNumber: string;
  apartmentNumber: string;
  postCode: string;
}
