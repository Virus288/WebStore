import React from 'react';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';
import { CategoryHeader } from '../Generic/Header';
import { opacity } from '../Animation/Variables';
import { RenderProducts } from './RenderProductsList';

interface ProductProps {
  categoryType: string;
  categoryName: string;
}

export const Products: React.FC<ProductProps> = (props) => {
  return (
    <motion.div className="productsView" variants={opacity} initial="init" animate="visible" exit="exit">
      <CategoryHeader {...{ category: props.categoryName }} />
      <RenderProducts {...{ category: props.categoryType }} />
    </motion.div>
  );
};

Products.propTypes = {
  categoryType: PropTypes.string.isRequired,
  categoryName: PropTypes.string.isRequired,
};
