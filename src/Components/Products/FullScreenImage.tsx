import React from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import { opacity } from '../Animation/Variables';

interface FullScreenImageInterface {
  src: string | undefined;
  closeImage: () => void;
}

export const FullScreenImage: React.FC<FullScreenImageInterface> = (image) => {
  return (
    <AnimatePresence exitBeforeEnter>
      <motion.img
        className="fullScreenImage"
        src={image.src}
        variants={opacity}
        initial="init"
        animate="visible"
        exit="exit"
      />
      <i className="icon-cancel closeProductView" onClick={(): void => image.closeImage()}>
        {' '}
      </i>
    </AnimatePresence>
  );
};
