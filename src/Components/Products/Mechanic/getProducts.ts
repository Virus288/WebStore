import { getAllProducts, getOneProduct } from '../Handlers/productsHandlers';
import { ProductsInterface } from '../Types/productsInterface';

export const handleGetProducts: (category: string) => Promise<ProductsInterface[] | void> = (category) => {
  return getAllProducts(category)
    .then((response) => {
      return response;
    })
    .catch((err) => console.log(err));
};

export const handleGetProduct: (category: string, id: string) => Promise<ProductsInterface | void> = (category, id) => {
  return getOneProduct(category, id)
    .then((response) => {
      return response;
    })
    .catch((err) => console.log(err));
};
