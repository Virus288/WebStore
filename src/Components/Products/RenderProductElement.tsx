import React, { useState } from 'react';
import { rootState } from '../../index';
import { useDispatch, useSelector } from 'react-redux';
import { AnimatePresence, motion } from 'framer-motion';
import { slideLeft } from '../Animation/Variables';
import { addBasket, disablePopup, disableProductView, enablePopup } from '../../Redux/actions';
import { GenericActionInterface } from '../../Redux/types/reduxTypes';

export const ProductView: React.FC = () => {
  const productStatus = useSelector((state: rootState) => state.product.toggle);
  const productProperties = useSelector((state: rootState) => state.product.product);

  // const [fullScreenImage, setFullScreenImage] = useState<boolean>(false);
  const [image, setImage] = useState<string>();

  const dispatch = useDispatch();

  const renderProductView = (): JSX.Element => {
    if (productProperties !== null) {
      return (
        <>
          <i
            className="icon-cancel closeProductView"
            onClick={(): GenericActionInterface => dispatch(disableProductView())}
          >
            {' '}
          </i>
          <img className="productViewMainImage" src={image === undefined ? productProperties.image.mainImage : image} />
          {/* <img className='productViewMainImage' src={image === undefined ? productProperties.image.mainImage : image} onClick={() => setFullScreenImage(true)} /> */}
          <div className="productViewImages">{renderImages(productProperties.image.additionalImages)}</div>
          <h1 className="productViewProductName">{productProperties.name}</h1>
          <h2 className="productViewProductPrice">{productProperties.price} zł</h2>
          <h2>{productProperties.fullDescription}</h2>
          <h2>Rozmiar: {productProperties.size}</h2>
          <h2>Kolor: {productProperties.color}</h2>
          <button
            className="productDivBuyButton"
            onClick={(): void =>
              addToBasket(
                productProperties._id,
                productProperties.name,
                productProperties.image.icon,
                productProperties.size,
                productProperties.price,
              )
            }
          >
            Dodaj do koszyka
          </button>
        </>
      );
    } else {
      return <h2>Nie wybrano produktu</h2>;
    }
  };

  const renderImages = (images: Array<string>): JSX.Element[] => {
    return images.map((image) => {
      return <img key={image} className="productViewProductImages" src={image} onClick={(): void => setImage(image)} />;
    });
  };

  const addToBasket = (id: string, name: string, image: string, size: string, price: number): void => {
    dispatch(addBasket(id, name, image, size, price));
    dispatch(enablePopup('Dodano do koszyka'));
    setTimeout(() => {
      dispatch(disablePopup());
    }, 2500);
  };

  return (
    <AnimatePresence exitBeforeEnter>
      {/* {fullScreenImage? <FullScreenImage {...{src: image, closeImage: () => setFullScreenImage(false)}}/> : ""} */}
      {productStatus ? (
        <motion.div className="productViewDiv" variants={slideLeft} initial="init" animate="visible" exit="exit">
          {renderProductView()}
        </motion.div>
      ) : (
        ' '
      )}
    </AnimatePresence>
  );
};
