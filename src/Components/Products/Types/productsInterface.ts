export interface productsInterface {
  _id: string;
  name: string;
  price: number;
  shortDescription: string;
  fullDescription: string;
  size: string;
  type: string;
  color: string;
  amount: number;
  image: imageInterface;
}

interface imageInterface {
  icon: string;
  mainImage: string;
  additionalImages: Array<string>;
}

export interface RenderDataInterface {
  category: string;
}

export interface ProductsInterface {
  _id: string;
  name: string;
  price: number;
  type: string;
  amount: number;
  size: string;
  color: string;
  fullDescription: string;
  shortDescription: string;
  image: imageInterface;
}
