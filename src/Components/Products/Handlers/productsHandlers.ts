import { ProductsInterface } from '../Types/productsInterface';

export const getAllProducts = async (category: string): Promise<ProductsInterface[]> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/produkty/${category}`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
  });
  const postData = await res.json();
  if (res.ok) {
    return postData;
  } else {
    throw res;
  }
};

export const getOneProduct = async (category: string, id: string): Promise<ProductsInterface> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/produkty/${category}/${id}`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
  });
  const postData = await res.json();
  if (res.ok) {
    return postData;
  } else {
    throw res;
  }
};
