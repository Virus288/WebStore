import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AnimatePresence, motion } from 'framer-motion';
import { opacity } from '../Animation/Variables';
import { addBasket, disablePopup, enablePopup, enableProductView } from '../../Redux/actions';
import { rootState } from '../../index';
import { getAllProducts } from './Handlers/productsHandlers';
import { ErroHandlerElement } from '../../Errors/ErrorHandlerView';
import { errorCodesHandler } from '../../Errors/errorCodesHandler';
import { ProductsInterface, RenderDataInterface } from './Types/productsInterface';
import { ProductView } from './RenderProductElement';
import { ProductActionInterface } from '../../Redux/reducers/product';

export const RenderProducts: React.FC<RenderDataInterface> = (data) => {
  const dispatch = useDispatch();

  const [products, setProducts] = useState<Array<ProductsInterface> | null>(null);
  const errors = useSelector((state: rootState) => state.errors.isError);

  useEffect(() => {
    getAllProducts(data.category)
      .then((products) => setProducts(products))
      .catch((error) => {
        errorCodesHandler(error, dispatch);
      });
  }, [data.category]);

  const addToBasket = (id: string, name: string, image: string, size: string, price: number): void => {
    dispatch(addBasket(id, name, image, size, price));
    dispatch(enablePopup('Dodano do koszyka'));
    setTimeout(() => {
      dispatch(disablePopup());
    }, 2500);
  };

  const renderProducts = (): JSX.Element[] | string => {
    if (products === null) {
      return 'Ładowanie...';
    } else {
      if (Object.keys(products).length > 0) {
        return products.map((product) => {
          return (
            <div className="productsDiv" key={product._id}>
              <img
                src={product.image.mainImage}
                onClick={(): ProductActionInterface => dispatch(enableProductView(product))}
                alt="image"
              />
              <h2 className="productDivName">{product.name}</h2>
              <h3 className="productDivPrice">{product.price} zł</h3>
              <h3 className="productDivShortDescription">{product.shortDescription}</h3>
              <button
                className="productDivBuyButton"
                onClick={(): void =>
                  addToBasket(product._id, product.name, product.image.icon, product.size, product.price)
                }
              >
                Dodaj do koszyka
              </button>
              <button
                className="productDivCheckButton"
                onClick={(): ProductActionInterface => dispatch(enableProductView(product))}
              >
                Sprawdź
              </button>
            </div>
          );
        });
      } else {
        return 'Brak wybranych produktów';
      }
    }
  };

  // <i className="icon-heart likeButton" style={{color: "#ff1c1c"}}></i>
  // const role = useSelector((state) => state.isLogged.role) === "admin";

  return (
    <AnimatePresence exitBeforeEnter>
      <motion.div className="productListOutterDiv" variants={opacity} initial="init" animate="visible" exit="exit">
        {' '}
        {errors ? <ErroHandlerElement /> : renderProducts()}
        <ProductView />
      </motion.div>
    </AnimatePresence>
  );
};
