import { BasketFinishInterface } from '../Interfaces/basketInterface';

export const sendFinishedPurchase = async (productsData: BasketFinishInterface): Promise<void> => {
  const res = await fetch(`${process.env.REACT_APP_BACKEND}/kup`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    credentials: 'include',
    body: JSON.stringify(productsData),
  });
  const postData = await res.json();
  if (res.ok) {
    return postData;
  } else {
    throw res;
  }
};
