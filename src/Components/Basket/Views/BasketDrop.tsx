import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { basketProductsInterface } from '../../../Redux/reducers/basket';
import { removeBasket } from '../../../Redux/actions';
import { Dispatch } from 'redux';
import { rootState } from '../../..';
import { BasketPayloadInterface } from '../../../Redux/types/reduxTypes';

export const BasketDrop: React.FC = () => {
  const dispatch = useDispatch();
  const basketItems = useSelector((state: rootState) => state.basket);

  return (
    <div className="basketDrop">
      <Link to="/koszyk" style={{ textDecoration: 'none' }}>
        <div className="checkBasketText">
          <h5>Sprawdź pełny koszyk</h5>
        </div>
      </Link>
      {renderBasketItems(basketItems, dispatch) ?? ''}
    </div>
  );
};

const renderBasketItems = (
  basketItems: Array<basketProductsInterface>,
  dispatch: Dispatch<BasketPayloadInterface>,
): JSX.Element[] => {
  return basketItems.map((item) => {
    return (
      <span key={item._id} id={item._id} className="basketItem">
        <img src={item.image} alt="img" />
        <h5>{item.name}</h5>
        <h4 onClick={(): BasketPayloadInterface => dispatch(removeBasket(item._id))}>X</h4>
      </span>
    );
  });
};
