import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { rootState } from '../../../index';
import { disablePopup, enablePopup } from '../../../Redux/actions';
import { BasketSubmitInterface } from '../Interfaces/basketInterface';

export const BasketSubmit: React.FC<BasketSubmitInterface> = (data) => {
  const dispatch = useDispatch();
  const isLogged = useSelector((state: rootState) => state.isLogged.role !== 'notLogged');

  const disableProductsEdit = (): void => {
    const inputs = document.querySelectorAll<HTMLInputElement>('.productBasketInput');
    const buttons = document.querySelectorAll<HTMLButtonElement>('.BasketProductButton');
    inputs.forEach((input) => {
      input.disabled = true;
    });
    buttons.forEach((button) => {
      button.classList.toggle('disabledButton');
      button.disabled = true;
    });
  };

  const checkIfEmpty = (): void => {
    let passed = true;
    const inputs = document.querySelectorAll<HTMLInputElement>('.productBasketInput');

    inputs.forEach((input): void => {
      if (input.value.length === 0 || input.value == '0') {
        passed = false;
        dispatch(enablePopup('Niektóre produkty nie posiadają ilości. Podaj ilość albo usuń je z koszyka'));
        setTimeout(() => {
          dispatch(disablePopup());
        }, 3500);
      }
    });
    if (passed) {
      disableProductsEdit();
      data.submitCategory('userData');
    }
  };

  return isLogged ? (
    <div className="basketSummaryDiv">
      <h2>Podsumowanie koszyka</h2>
      <span className="basketSummarySpan">
        <h2 className="basketAmount">0</h2>
        <h2> zł</h2>
        <button onClick={(): void => checkIfEmpty()}>Kup teraz</button>
      </span>
    </div>
  ) : (
    <div className="basketSummaryDiv">
      <h2>Nie jesteś zalogowany. W celu zakupienia produktów, zaloguj się</h2>
    </div>
  );
};
