import React, { useEffect, useState } from 'react';
import { BasketFinishInterface } from '../Interfaces/basketInterface';
import { sendFinishedPurchase } from '../Handlers/basketHandlers';

export const BasketFinish: React.FC<BasketFinishInterface> = (data) => {
  const [message, setMessage] = useState<boolean | undefined>(undefined);

  const renderMessage = (): string => {
    switch (message) {
      case undefined:
        return 'Ładowanie...';
      case true:
        return 'Produkty zostały zarezerwowane dla państwa. Aktualnie jedyna możliwość dokonania wpłaty to przelew na konto bankowe. Numer zostanie podany kiedy sklep będzie w pełni działający.';
      case false:
        return 'Wystąpił błąd. Pracujemy nad jego rozwiązaniem';
      default:
        return 'Ładowanie...';
    }
  };

  useEffect(() => {
    try {
      sendFinishedPurchase(data).then((data: any) => setMessage(data));
    } catch (err) {
      console.log(err);
    }
  });

  return (
    <div className="basketSummaryDiv">
      <h2>{renderMessage()}</h2>
    </div>
  );
};
