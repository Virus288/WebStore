import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { rootState } from '../../../index';
import { removeBasket } from '../../../Redux/actions';
import { BasketUserData } from './UserAdress';
import { BasketSubmit } from './BasketSubmit';
import { BasketFinish } from './BasketFinish';
import { basketProductsInterface } from '../../../Redux/reducers/basket';
import { CategoryHeader } from '../../Generic/Header';
import { BasketInterface, UserDataInterface } from '../Interfaces/basketInterface';

export const BasketComponent: React.FC = () => {
  const dispatch = useDispatch();

  const basketItems = useSelector((state: rootState) => state.basket);
  const [basketSubmit, setBasketSubmit] = useState<Array<BasketInterface>>();
  const [userData, setUserData] = useState<UserDataInterface>();
  const [component, setComponent] = useState<string>('basket');

  useEffect(() => {
    if (component === 'basket') {
      renderBasketSummary();
    }
  }, [basketSubmit, component]);

  const renderComponents = (): JSX.Element => {
    switch (component) {
      case 'basket':
        return <BasketSubmit {...{ submitCategory: setComponent }} />;
      case 'userData':
        return <BasketUserData {...{ submitCategory: setComponent, userData: setUserData }} />;
      case 'finished':
        return <BasketFinish {...{ userData: userData, products: basketSubmit }} />;
      default:
        return <BasketSubmit {...{ submitCategory: setComponent }} />;
    }
  };

  const submitPrice = (amount: number, price: number, _id: string): void => {
    if (amount == 0 || isNaN(amount)) {
      if (basketSubmit !== undefined) {
        const indexOfItem = basketSubmit.findIndex((productData) => productData._id == _id);
        if (indexOfItem !== -1) {
          const newBasket = basketSubmit.filter((product) => product._id !== _id);
          setBasketSubmit(newBasket);
        }
      }
    } else {
      if (basketSubmit !== undefined) {
        const indexOfItem = basketSubmit.findIndex((productData) => productData._id == _id);
        if (indexOfItem !== -1) {
          const newItems = basketSubmit;
          newItems[indexOfItem] = { amount, price, _id };
          setBasketSubmit(newItems);
        } else {
          const newBasket = [...basketSubmit, { amount, price, _id }];
          setBasketSubmit(newBasket);
        }
      } else {
        setBasketSubmit([{ amount, price, _id }]);
      }
    }
    renderBasketSummary();
  };

  const renderBasket = (basket: Array<basketProductsInterface>): JSX.Element[] | JSX.Element => {
    if (basket.length === 0) {
      return <h2>Brak przedmiotów w koszyku</h2>;
    } else {
      return basket.map((product) => {
        return (
          <div className="basketProductDiv" key={product._id}>
            <span className="productName">
              <img src={product.image} />
              <h2 className="productDivName">{product.name}</h2>
            </span>
            <h3 className="productDivPrice">{product.price} zł</h3>
            <h3 className="productDivShortDescription">Rozmiar: {product.size}</h3>
            <span>
              <h3 className="productDivAmount">Ilość: </h3>
              <input
                className="productBasketInput"
                type="number"
                min="0"
                onChange={(e): void => submitPrice(parseInt(e.target.value), product.price, product._id)}
              />
            </span>
            <button
              className="BasketProductButton"
              onClick={(): void => {
                submitPrice(0, product.price, product._id);
                dispatch(removeBasket(product._id));
              }}
            >
              Usuń z koszyka
            </button>
          </div>
        );
      });
    }
  };

  const renderBasketSummary = (): void => {
    if (basketSubmit !== undefined) {
      let newBasketAmount = 0;
      basketSubmit.forEach((product) => {
        newBasketAmount = newBasketAmount + product.amount * product.price;
      });
      const amount = document.querySelector('.basketAmount') as HTMLElement;
      amount.innerHTML = String(newBasketAmount);
    }
  };

  return (
    <>
      <CategoryHeader {...{ category: 'Koszyk' }} />
      <div className="basketComponentOuterDiv">
        <div className="basketProductsDiv">{renderBasket(basketItems)}</div>
        {basketItems !== undefined ? (basketItems.length > 0 ? renderComponents() : '') : ''}
      </div>
    </>
  );
};
