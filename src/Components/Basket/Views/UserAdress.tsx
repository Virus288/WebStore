import React, { useState } from 'react';

import { BasketSubmitInterface, UserDataInterface } from '../Interfaces/basketInterface';

export const BasketUserData: React.FC<BasketSubmitInterface> = (userFunctions) => {
  const [data, setData] = useState<UserDataInterface>({
    miejscowosc: undefined,
    ulica: undefined,
    budynek: undefined,
    lokal: undefined,
    kodPocztowy: undefined,
  });

  const submitForm = (e: React.FormEvent<EventTarget>): void => {
    e.preventDefault();
    if (data.budynek !== undefined || data.ulica !== undefined || data.kodPocztowy !== undefined) {
      if (userFunctions.userData !== undefined) {
        userFunctions.userData(data);
        userFunctions.submitCategory('finished');
      }
    }
  };

  const handleInput = (type: string, value: string): void => {
    setData({
      ...data,
      [type]: value,
    });
  };

  return (
    <div className="basketSummaryDiv">
      <form onSubmit={(e): void => submitForm(e)} className="userDataForm">
        <label htmlFor="miejscowosc">Miejscowość</label>
        <input
          type="text"
          name="miejscowosc"
          placeholder="Miejscowość"
          required
          onChange={(e): void => {
            const input = e.target as HTMLInputElement;
            handleInput(input.name, input.value);
          }}
        />

        <label htmlFor="ulica">Ulica</label>
        <input
          type="text"
          name="ulica"
          placeholder="Ulica"
          required
          onChange={(e): void => {
            const input = e.target as HTMLInputElement;
            handleInput(input.name, input.value);
          }}
        />

        <label htmlFor="budynek">Numer budynku</label>
        <input
          type="text"
          name="budynek"
          placeholder="Numer budynku"
          required
          onChange={(e): void => {
            const input = e.target as HTMLInputElement;
            handleInput(input.name, input.value);
          }}
        />

        <label htmlFor="lokal">Numer lokalu</label>
        <input
          type="text"
          name="lokal"
          placeholder="Numer lokalu"
          onChange={(e): void => {
            const input = e.target as HTMLInputElement;
            handleInput(input.name, input.value);
          }}
        />

        <label htmlFor="kodPocztowy">Kod pocztowy</label>
        <input
          type="text"
          name="kodPocztowy"
          placeholder="Kod pocztowy"
          required
          onChange={(e): void => {
            const input = e.target as HTMLInputElement;
            handleInput(input.name, input.value);
          }}
        />

        <button>Zatwierdź</button>
      </form>
    </div>
  );
};
