export interface BasketInterface {
  amount: number;
  price: number;
  _id: string;
}

export interface BasketSubmitInterface {
  submitCategory: (category: string) => void;
  userData?: (data: UserDataInterface) => void;
}

export interface BasketFinishInterface {
  userData: UserDataInterface | undefined;
  products: BasketInterface[] | undefined;
}

export interface UserDataInterface {
  miejscowosc: string | undefined;
  ulica: string | undefined;
  budynek: string | undefined;
  lokal: string | undefined;
  kodPocztowy: string | undefined;
}
