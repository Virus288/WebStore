import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import './Css/style.css';
import './Css/fontello/css/fontello.css';
import { Routers } from './Routers/Router';
import { Navbar } from './Components/Generic/Navbar';
import { Popup } from './Components/Generic/Popup';
import { handleCheckIfLogged } from './Components/Account/NotLoggedUserComponents/Mechanic/loginFunctions';

export const App: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    handleCheckIfLogged(dispatch);
  });

  return (
    <Router>
      <div className="App ">
        <Navbar />
        <Popup />
        <div className="content ">
          <Routers />
        </div>
      </div>
    </Router>
  );
};
