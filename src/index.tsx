import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import reportWebVitals from './reportWebVitals';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './Redux/reducers/index';
import 'normalize.css';

export const store = createStore(rootReducer);
export type rootState = ReturnType<typeof store.getState>;
export type reducersState = ReturnType<typeof store.dispatch>;

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root'),
);

reportWebVitals();
