import { GenericActionInterface, LogInActionInterface } from '../types/reduxTypes';
import { UserAccountData } from '../../Components/Account/LoggedUserComponents/Types/loginTypes';
import { productsInterface } from '../../Components/Products/Types/productsInterface';
import { UserDataPayloadInterface } from '../reducers/userDataReducer';
import { BasketActionInterface, BasketPayloadInterface } from '../reducers/basket';
import { ProductActionInterface } from '../reducers/product';
import { PopupActionInterface } from '../reducers/popup';
import { ErrorActionInterface } from '../reducers/errors';
import { ErrorCodes } from '../../Errors/errorCodesHandler';

export const logIn = (role: string, verified: boolean, username: string): LogInActionInterface => {
  return {
    type: 'SING_IN',
    payload: { role, verified, username },
  };
};

export const logOut = (): GenericActionInterface => {
  return {
    type: 'SING_OUT',
  };
};

export const addBasket = (
  id: string,
  name: string,
  img: string,
  size: string,
  price: number,
): BasketActionInterface => {
  return {
    type: 'ADD_TO_BASKET',
    payload: { id, name, img, size, price },
  };
};

export const removeBasket = (id: string): BasketPayloadInterface => {
  return {
    type: 'REMOVE_FROM_BASKET',
    payload: { id },
  };
};

export const clearBasket = (): GenericActionInterface => {
  return {
    type: 'REMOVE_WHOLE_BASKET',
  };
};

export const enablePopup = (text: string): PopupActionInterface => {
  return {
    type: 'ENABLE_POPUP',
    payload: { text },
  };
};

export const disablePopup = (): GenericActionInterface => {
  return {
    type: 'DISABLE_POPUP',
  };
};

export const addError = (message: string | Error | ErrorCodes): ErrorActionInterface => {
  return {
    type: 'ADD_ERROR',
    payload: { message },
  };
};

export const removeError = (): GenericActionInterface => {
  return {
    type: 'CLEAN_ERRORS',
  };
};

export const enableProductView = (product: productsInterface): ProductActionInterface => {
  return {
    type: 'ENABLE_PRODUCT_VIEW',
    payload: { product },
  };
};

export const disableProductView = (): GenericActionInterface => {
  return {
    type: 'DISABLE_PRODUCT_VIEW',
  };
};

export const addUserData = (data: UserAccountData): UserDataPayloadInterface => {
  return {
    type: 'ADD_USER_DATA',
    payload: {
      email: data.email,
      city: data.city,
      street: data.street,
      buildingNumber: data.buildingNumber,
      apartmentNumber: data.apartmentNumber,
      postCode: data.postCode,
    },
  };
};

// export const addFav = (id: string, name: string, img: string, size: string, price: number) => {
//     return {
//         type: "ADD_TO_FAVS",
//         payload: {id, name, img, size, price}
//     };
// };

// export const removeFav = (id: string) => {
//     return {
//         type: "REMOVE_FROM_FAVS",
//         payload: {id}
//     };
// };
