export interface LogInActionInterface {
  payload: {
    role: string;
    verified: boolean;
    username: string;
  };
  type: string;
}

export interface BasketPayloadInterface {
  type: string;
  payload: { id: string };
}

export interface GenericActionInterface {
  type: string;
}
