import { UserAccountData } from '../../Components/Account/LoggedUserComponents/Types/loginTypes';

interface UserDataReducerInterface extends UserAccountData {
  loaded: boolean;
}

export interface UserDataPayloadInterface {
  type: string;
  payload: UserAccountData;
}

export const userDataReducer = (
  state: UserDataReducerInterface = {
    email: '',
    postCode: '',
    apartmentNumber: '',
    buildingNumber: '',
    street: '',
    city: '',
    loaded: false,
  },
  action: UserDataPayloadInterface,
): UserDataReducerInterface => {
  switch (action.type) {
    case 'ADD_USER_DATA':
      return (state = { ...action.payload, loaded: true });
    default:
      return state;
  }
};
