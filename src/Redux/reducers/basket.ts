export interface basketProductsInterface {
  _id: string;
  name: string;
  price: number;
  size: string;
  image: string;
}

interface basketReducerInterface {
  payload: {
    id: string;
    name: string;
    img: string;
    size: string;
    price: number;
  };
  type: string;
}

export interface BasketPayloadInterface {
  type: string;
  payload: { id: string };
}

export interface BasketActionInterface {
  type: string;
  payload: { id: string; name: string; img: string; size: string; price: number };
}

export const basketReducer = (
  state: basketProductsInterface[] = [],
  action: basketReducerInterface,
): basketProductsInterface[] => {
  switch (action.type) {
    case 'ADD_TO_BASKET':
      if (state.find((element) => element._id === action.payload.id) === undefined) {
        return (state = [
          ...state,
          {
            _id: action.payload.id,
            name: action.payload.name,
            image: action.payload.img,
            size: action.payload.size,
            price: action.payload.price,
          },
        ]);
      } else {
        return state;
      }
    case 'REMOVE_FROM_BASKET':
      return (state = state.filter((item) => {
        return item._id !== action.payload.id;
      }));
    case 'REMOVE_WHOLE_BASKET':
      return (state = []);
    default:
      return state;
  }
};
