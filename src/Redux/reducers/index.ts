import { combineReducers } from 'redux';
import { loggedReducer } from './isLogged';
import { basketReducer } from './basket';
import { popupReducer } from './popup';
import { errorsReducer } from './errors';
import { productReducer } from './product';
import { userDataReducer } from './userDataReducer';

const rootReducer = combineReducers({
  isLogged: loggedReducer,
  popup: popupReducer,
  basket: basketReducer,
  errors: errorsReducer,
  product: productReducer,
  userData: userDataReducer,
});

export default rootReducer;
