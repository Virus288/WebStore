interface popupInterface {
  payload: {
    toggle: boolean;
    text: string | null;
  };
  type: string;
}

interface stateInterface {
  toggle: boolean;
  text: string | null;
}

export interface PopupActionInterface {
  type: 'ENABLE_POPUP';
  payload: { text: string };
}

export const popupReducer = (
  state: stateInterface = {
    toggle: false,
    text: null,
  },
  action: popupInterface,
): stateInterface => {
  switch (action.type) {
    case 'ENABLE_POPUP':
      return (state = { toggle: true, text: action.payload.text });
    case 'DISABLE_POPUP':
      return (state = { toggle: false, text: null });
    default:
      return state;
  }
};
