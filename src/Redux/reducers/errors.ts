import { ErrorCodes } from '../../Errors/errorCodesHandler';

interface errorsInterface {
  isError: boolean;
  message: string | null;
}

interface errorsReducerInterface {
  payload: {
    message: string;
  };
  type: string;
}

export interface ErrorActionInterface {
  type: 'ADD_ERROR';
  payload: { message: string | Error | ErrorCodes };
}

export const errorsReducer = (
  state: errorsInterface = { isError: false, message: null },
  action: errorsReducerInterface,
): errorsInterface => {
  switch (action.type) {
    case 'ADD_ERROR':
      return (state = {
        isError: true,
        message: action.payload.message,
      });
    case 'CLEAN_ERRORS':
      return (state = {
        isError: false,
        message: null,
      });
    default:
      return state;
  }
};
