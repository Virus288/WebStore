interface action {
  type: string;
  payload: {
    role: string;
    verified: boolean;
    username: string;
  };
}

interface stateInterface {
  role: string;
  verified: boolean;
  username: string;
}

export const loggedReducer = (
  state: stateInterface = { role: 'notLogged', verified: false, username: '' },
  action: action,
): stateInterface => {
  switch (action.type) {
    case 'SING_IN':
      return (state = {
        role: action.payload.role,
        verified: action.payload.verified,
        username: action.payload.username,
      });
    case 'SING_OUT':
      return (state = {
        role: 'notLogged',
        verified: false,
        username: '',
      });
    default:
      return state;
  }
};
