import { productsInterface } from '../../Components/Products/Types/productsInterface';

interface LocalProductInterface {
  payload: {
    toggle: boolean;
    product: productsInterface;
  };
  type: string;
}

interface stateInterface {
  toggle: boolean;
  product: productsInterface | null;
}

export interface ProductActionInterface {
  type: string;
  payload: { product: productsInterface };
}

export const productReducer = (
  state: stateInterface = { toggle: false, product: null },
  action: LocalProductInterface,
): stateInterface => {
  switch (action.type) {
    case 'ENABLE_PRODUCT_VIEW':
      return (state = { toggle: true, product: action.payload.product });
    case 'DISABLE_PRODUCT_VIEW':
      return (state = { toggle: false, product: null });
    default:
      return state;
  }
};
