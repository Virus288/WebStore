import { Dispatch } from 'redux';
import { addError } from '../Redux/actions';

export interface ErrorCodes {
  errCode: number;
  error: string;
  errors: any;
}

export const errorCodesHandler = (error: ErrorCodes, dispatch: Dispatch): string | Error | ErrorCodes => {
  let localErr;
  switch (error.errCode) {
    case 400:
      localErr = genericCodesHandler(error.error);
      break;
    case 401:
      localErr = dataAccessCodesHandler(error.error, error.errors);
      break;
    case 404:
      localErr = 'Source does not exist';
      break;
    case 500:
      localErr = 'It seems that server is down. Try again later';
      break;
    default:
      console.log({ message: 'Error: Unknown error', error: error });
      localErr = error;
      break;
  }
  dispatch(addError(localErr));
  return localErr;
};

const genericCodesHandler = (error: string): string | Error => {
  switch (error) {
    case 'Wrong type of data':
      console.log('Error: Wrong type of data');
      return 'Wrong type of data';
    default:
      console.log({ message: 'Error: Unknown error', error: error });
      return error;
  }
};

const dataAccessCodesHandler = (error: string, errors: Error): string | Error => {
  switch (error) {
    case 'Authorization failed':
      return errors;
    default:
      console.log({ message: 'Error: Unknown error', error: error });
      return error;
  }
};
