import React from 'react';

export const ErroHandlerElement: React.FC = () => {
  return (
    <div>
      <h2 className="errorDivText">Błąd serwera. Spróbuj odczekać chwilę po czym odświeżyć stronę</h2>
    </div>
  );
};
