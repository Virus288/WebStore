export interface ErrorsInterface {
  errors: { [key: string]: string };
  error: string;
}
